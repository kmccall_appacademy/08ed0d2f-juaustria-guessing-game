def guessing_game
  comp_set = rand(1..100)
  user_guess = 0
  guess_count = 0
  until user_guess == comp_set
    puts 'Please guess a number'
    user_guess = gets.chomp.to_i
    puts "Your guess #{user_guess} was"
    if user_guess > comp_set
      puts 'too high.'
    elsif user_guess < comp_set
      puts 'too low.'
    end
    guess_count += 1
  end
  puts "#{user_guess} is the correct number"
  puts "It took you #{guess_count} guesses"
end
