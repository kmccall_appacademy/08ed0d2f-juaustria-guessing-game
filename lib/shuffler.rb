puts 'Please input a file'
input = gets.chomp
input_name = input[0...-4]
to_shuffle = []
File.foreach(input) do |line|
  to_shuffle << line
end
shuffled = to_shuffle.shuffle
File.open("#{input_name}--shuffled.txt", 'w') do |f|
  shuffled.each do |line|
    f << line
  end
end
